package com.example.mapproject;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Page2 extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page2);
		 Button nextbutton = (Button)findViewById(R.id.button1);
	     nextbutton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent yeni=new Intent(Page2.this, harita.class);
				startActivity(yeni);
				
			}
		});
	     Button deneme = (Button)findViewById(R.id.button2);
	     final EditText edittext = (EditText) findViewById(R.id.editText1);
	     
	     deneme.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),edittext.getText(), Toast.LENGTH_LONG).show();
			}
		});
	
	}

}
